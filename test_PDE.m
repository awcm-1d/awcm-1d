%THIS SCRIPT TESTS THE DERIVATIVES USING WAVELET TRANSFORM WFT ON AN INTERVAL
% Written: Oleg V. Vasilyev, 14 October, 2018

clear all;
global ende XX L G j_df Type jlev Jmx scl acc order h hh der_order nu


Type        = 'periodic';                         % Domain type: 'interval' or 'periodic'
cl          = 2^2;                                %RESOLUTION ON COARSEST LEVEL
jlev        = 4;                                  %NUMBER OF LEVELS
Jmx         = 8;                                  %MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1+cl*2^(jlev-1);                    %RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
ende_mx     = 1+cl*2^(Jmx-1);                     %RESOLUTION ON FINEST LEVEL FOR NON-PERIODIC CASE
order       = 3;
der_order   = 3;
acc         = 0.01;                              %ACCURACY CONTROL OVER WAVELET AMPLITUDES
CFL         = 0.1;                               % CFL
Tend        = 1.0;                                % time interval
dt          = 1.e-03;                             % initial time step
nu          = 0.01;                               % viscosity
scl_floor   = 0.1;                                % scl = max(scl, scl_floor)
switch lower(Type)
    case 'periodic'
        ende    = ende - 1;
        ende_mx = ende_mx - 1;      
end
L           = logical(ones(1,ende));

hh      = 1.0/(ende_mx-mod(ende_mx,2));        %hh - mesh spacing on the finest level
XX      = hh*[0:ende_mx-1];                    %XX - grid on finest level

%SPECIFY SIGNAL
s      = 2^(Jmx-jlev);
h      = hh*s;                           %h - mesh spacing on the finest level
X      = XX(1:s:end);                    %X - grid on finest level

%INITIAL CONDITIONS
for iter = 1:Jmx-jlev+1
    Fn     = IC(X);
    Fn(~L) = NaN;
    scl    = max(0.5*(max(Fn(L))-min(Fn(L))),scl_floor);
    
    %ADAPT GRID
    [Fn, X, L]  = adapt_grid(Fn, L);
        
    Fn(L)  = IC(X(L));
    Fn(~L) = NaN;

end

%Time advancement
clf;
t = 0;
while t < Tend
   
    dt =  min(1.5*dt,CFL*h/max(Fn(L)));
    t = t + dt;
    
    
    options = odeset('RelTol',1e-1*acc,'AbsTol',1e-1*acc,'MaxStep',dt,'InitialStep',dt,'MaxOrder',2);
    [tout,y] = ode15s(@rhs, [0 dt], Fn(L),options);
    Fn(L) = y(end,:);
         
   %Fn(L) = Fn(L) + dt * rhs(t,Fn(L));  %Euler time integration

    %Boundary Conditions
    switch lower(Type)
        case 'interval'
            i=find(L);
            [Fn(i(1)),Fn(i(end))] = BC('bc');
    end
    
    %ADAPT GRID
    [Fn, X, L]  = adapt_grid(Fn, L);
    
    %PLOT SOLUTION
    figure(1);
    subplot(211);
    hold off;
    plot(X(L), Fn(L), 'k-');
    axis([0, 1, -1, 1]);
    hold on
    plot(X(L), Fn(L), 'rx');
    title(['Burgers Equation, ' sprintf('dt=%0.2e, t=%0.3f',dt,t)]);
    Achsen  = axis;
    axis([0, 1, Achsen(3:4)]);
    
    %VISUALIZE TREE ===================================================================================================================
    subplot(212)
    hold off;
    Level   = jlev*ones(size(Fn));
    for j = jlev-1:-1:1
        s = 2^(jlev-j-1);
        Level(1:2*s:ende)   = j;
    end
    plot(X(G), Level(G), 'bo');
    hold on
    plot(X(L), Level(L), 'rx');
    set(gca, 'YTick', [1:jlev]);
    axis([0, 1, 0, jlev+1]);
    title(['Original jlev: ', num2str(jlev), '  New jlev: ', num2str(jlev), ' Using ', num2str(100*sum(L)/ende) '% of points']); 
end

%===========================================================

%Initial Conditions
function f = IC(x)
    
    %f = zeros(size(x));
    f = sin(2.0*pi*x);

end

%Boundary Conditions
function [y_l,y_r] = BC(flag)

   switch lower(flag)
       case 'rhs'
           y_l=0.0;
           y_r=0.0;
       case 'bc'
           y_l=0.0;
           y_r=0.0;
   end

end

%Evolution Equation
function dydt = rhs(t,y)
global nu Type
 
   [x, dy, d2y] = diff_wlt(y);      
   dydt = ( - y .* dy + nu * d2y );% + 32*(0.25-x).*exp(-64*(x-0.25).^2);
   
   %Boundary Conditions
   switch lower(Type)
       case 'interval'
           [dydt(1),dydt(end)] = BC('rhs');
   end

end

