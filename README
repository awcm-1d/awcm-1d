Matlab 1-D Demo for Adaptive Wavelet Collocation Method (AWCM) (Vasilyev & Bowman, 2000)
Written by Oleg V. Vasilyev, 14 October, 2018

The distribution contains the following matlab files, essential for understanding of the basics of the AWCM method:
C_range_corr.m         - index correction for update stege of wavelet transform on an interval
D_range_corr.m         - index correction for predict stege of wavelet transform on an interval
G_range_corr.m         - index correction for derivative calculations on an interval
adapt_grid.m           - interal procedure for grid adaptation used to solve PDEs (high level call for convenience)
add_adjacent.m         - adds adjacent wavelets to the mask of significan coefficients
add_ghost.m            - creates a mask of ghost points and index of resolution levels for derivative calculation
adjust_lev.m           - adjusts level of resolution based on analysis of significant wavelet coefficients 
calc_derivative.m      - calculates derivative with on the fly calculation of the mask of ghost points and index of resolution levels 
calc_derivative_mask.m - same as calc_derivative but with precalculation of the mask of ghost points and index of resolution levels 
d2f_pt.m               - finite difference calculation of second derivatig on arbitrary stencil
d2wgh.m                - weights for finite difference calculation of the second derivatig on arbitrary stencil
df_pt.m                - finite difference calculation of first derivatig on arbitrary stencil
diff_wlt.m             - calciulates derivative using global definitions (high level call for convenience)
dwgh.m                 - weights for finite difference calculation of the first derivatig on arbitrary stencil
prd_corr.m             - periodic index correction that wraps the points around
reconstruction_check.m - check if all the supporting wavelets required for adaptive wavelet transform are present and does the correction if necessary.
significant.m          - creates the mask of significant points
test_PDE.m             - script to test soluion of PDE (example of Burger's equation)
test_adapt_grid.m      - script to test grid adaptation for both periodic and interval cases
test_calc_derivative.m - script to test derivative calculation for both periodic and interval cases
test_wtf_interval.m    - script to test wavelet transform on an interval
test_wtf_periodic.m    - script to test the periodic wavelet transform 
wgh.m                  - weights for predict and update stages of wavelet transform for arbitrary stencil
wtf.m                  - wavelet transform 1-d second generation wavelet transform on adaptive grid

All demo files are written for clarity of understanding of the algorithm without any consideration for efficiency.

References:
Vasilyev, O.V. and Bowman, C., “Second Generation Wavelet Collocation Method for the Solution of Partial Differential Equations,” Journal of Computational Physics, 165, pp. 630–693, 2000.



