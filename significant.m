function L_sig = significant(Fn, L_in, scl, acc, jlev)
%WTF a 1-d second generation wavelet transform
%
% Fn   - is the function or the transformed function
% L_in - mask
% scl  - absolute scale for adaptation
% acc  - relative scale for adaptation
% jlev - number of jleves
% Jmx  - maximum allowable level of resolution

% Written: Oleg V. Vasilyev, 11 October, 2018

ende = length(Fn);
Fn(~L_in) = 0.;

%FORWARD WAVELET TRANSFORM - GO FROM THE FINEST (jlev-1) TO THE COARSEST LEVEL
L_sig       = logical( zeros(size(L_in)));      %Mask of significan wavelets - initialize to false.

%CHECK SIGNIFICANT WAVELETS
for j = jlev-1:-1:1
     %STRIDE
     s = 2^(jlev-j-1);
     
     %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
     D_ind = [1+s:2*s:ende];
     C_ind = [1:2*s:ende];
     
     D_ind = D_ind(find(L_in(D_ind)));
     C_ind = C_ind(find(L_in(C_ind)));
     
     %IF THE DIFFERENCE BETWEEN INTERPOLATED SIGNAL AND ORIGINAL SIGNAL WAS BIGGER THAN ACCURACY -> STORE POINT
     L_sig(D_ind)   = (abs(Fn(D_ind)))>=acc*scl | L_sig(D_ind);
     
     %FALG COURSEST LEVEL TRUE
     if j == 1 
         L_sig(C_ind) = true;
     end 
end
