function L_adj = add_adjacent(Type, L_in, jlev)
%ADD_ADJACENT adds the neighbours needed to catch where the energy goes to.
% 
% Neighbours are added on the same level and one level above.
%
% Type - 'internal' or 'periodic'
% L_in - mask of significan wavelet
% jlev - number of jleves (clev can be calculated from that)
%
% Written: Oleg V. Vasilyev, 14 October, 2018

ende       = length(L_in);
L_adj      = L_in;
      
switch lower(Type)
case 'interval'
    %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j = jlev:-1:1
        s = 2^(jlev-j);
        
        %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
        if j == 1
            S_ind = [1:s:ende];
        else
            S_ind = [1+s:2*s:ende]; 
        end
        S_ind = S_ind(find(L_in(S_ind)));
        
        %RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED       
        if length(S_ind) > 0 
            for i = [-1,1]*s/2^(min(j+1,jlev)-j)
                L_adj(S_ind(S_ind + i >=1 & S_ind + i <= ende)+i) =  true;
            end
        end
    end
    
case 'periodic'
        %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j = jlev:-1:1
        s = 2^(jlev-j);
        
        %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
        if j == 1
            S_ind = [1:s:ende];
        else
            S_ind = [1+s:2*s:ende]; 
        end
        S_ind = S_ind(find(L_in(S_ind)));
        
        %RECONSTRUCTION CHECK: IF ANY D IS FLAGGED THEN IT'S NEIGBOURS ON THE LEVEL, REQUIRED FOR PREDICT STAGE, MUST BE FLAGGED       
        if length(S_ind) > 0 
            for i = [-1,1]*s/2^(min(j+1,jlev)-j)
                L_adj(prd_corr(S_ind + i,ende)) =  true;
            end
        end
    end
end
    