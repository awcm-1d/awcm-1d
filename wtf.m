function Fn = wtf(Type, Fn, jlev, order, Flag, L_in)
%WTF a 1-d second generation wavelet transform
%
% Fn   - is the function or the transformed function
% jlev - number of jleves (clev can be calculated from that)
% L_in - in case Fn has been truncated than L_in knows how to translate Fn_trunc -> Fn, default logical(ones(size(Fn)))
% Flag - 'forward' or 'inverse', default 'forward'
%
% EXPLANATION OF D's AND C'S
%
%    1           2         3
%  D(3) �      D(2)      D(3)  �
%       � C(1)      C(2)      C(1)
%           1         2         3
%
% Written: Oleg V. Vasilyev 14 October, 2018

%SET DEFAULT ACCURACY (THROW AWAY CRITERIUM)
order_def   = 1;
%CHECKING OF INPUT
if nargin==3
    order       = order_def;
    Flag        = 'forward';
    L_in        = logical(ones(size(Fn)));
elseif nargin==4
    Flag        = 'forward';
    L_in        = logical(ones(size(Fn)));
elseif nargin==5
    L_in        = logical(ones(size(Fn)));
elseif nargin~=6
    errordlg('Unknown no of input args!', 'WFT');
end

switch lower(Type)
case 'interval'
    %CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
    ende    = length(L_in);
    n_range = [-fix((order+1)/2):order-fix((order+1)/2)];
            
    switch lower(Flag)
    case 'forward' %==========================================================================================================================
        %MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
        Fn(~L_in)   = 0;
               
        %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        for j = jlev-1:-1:1
            %STRIDE
            s = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(L_in(D_ind)));
            C_ind = C_ind(find(L_in(C_ind)));
            
            %PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for i = D_ind
                n_range_corr=D_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) - Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
            %UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for i = C_ind
                n_range_corr=C_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) + 0.5*Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
                       
        end
        
    case 'inverse' %==========================================================================================================================

        %MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
        Fn(~L_in)   = 0;
        
        %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        for j=1:jlev-1 
            %STRIDE
            s       = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(L_in(D_ind)));
            C_ind = C_ind(find(L_in(C_ind)));
            
            %INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for i = C_ind
                n_range_corr=C_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) - 0.5*Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
            %INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for i = D_ind
                n_range_corr=D_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) + Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
            
        end
        
        %SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
        Fn(~L_in)   = NaN;
    end
    
case 'periodic'
    %CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
    ende       = length(L_in);
    n_range = [-fix((order+1)/2):order-fix((order+1)/2)];
    
    switch lower(Flag)
    case 'forward' %==========================================================================================================================                
        %MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
        Fn(~L_in)   = 0;
        
        %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
        for j = jlev-1:-1:1                                                   
            %STRIDE
            s = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(L_in(D_ind)));
            C_ind = C_ind(find(L_in(C_ind)));
            
            %PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for k = 1:order+1
                Fn(D_ind) = Fn(D_ind) - Fn(prd_corr(D_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end
            %UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for k = 1:order+1
                Fn(C_ind) = Fn(C_ind) + 0.5*Fn(prd_corr(C_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end   
            
        end
        
        %REDUCE RESOLUTION ACCORDING TO ACCURACY CRITERIUM
        Fn(~L_in)  = NaN;
        
    case 'inverse' %==========================================================================================================================
        %MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
        Fn(~L_in)   = 0;
     
        for j = 1:jlev-1                                                   %LOOP THROUGH THE LEVELS
            s = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION 

            D_ind =  [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(L_in(D_ind)));
            C_ind = C_ind(find(L_in(C_ind)));
            
            %INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for k = 1:order+1
                Fn(C_ind) = Fn(C_ind) - 0.5*Fn(prd_corr(C_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end
            %INVERSE PREDICT STAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for k = 1:order+1
                Fn(D_ind) = Fn(D_ind) + Fn(prd_corr(D_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end
        end
        
        %SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
        Fn(~L_in)   = NaN;
    end
end