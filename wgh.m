function wgh = wgh(k, n_l, n_h)
%WGH calculates weights for uniformly spaced Lagrange interpolation to the half
%point X_{s-1/2} using X_l= l*Delta points (l=(s+n_l)...(s+n_h))
%
% k  - Index of the point
% nl - How many point do I want to use to the left  (could be to the right)
% nh - How many point do I want to use to the right (could be to the right)
%
% EXAMPLE
% wgh(0,   0, 1) - Calculates the weight in point  0 using first order extrapolation to point -1/2 from points 0,   1
% wgh(-1, -2, 1) - Calculates the weight in point -1 using third order intrapolation to point -1/2 from points -2, -1, 0, 1
%
% Written: Oleg V. Vasilyev, 4 October, 2018

range=[n_l:n_h];
i=range(find(range ~= k));
wgh=prod(i+1/2)/prod(i-k);

