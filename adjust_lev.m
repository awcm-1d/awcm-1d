function [F_out, L_out, jlev_out, ende] = adjust_lev(F_in, L_in, jlev_in, Jmx)
%ADJUST_LEV - adjusts level of resolution based on analysis of significant
%wavelet coefficients
%
% Fn   - is the function or the transformed function
% L_in - mask
% scl  - absolute scale for adaptation
% acc  - relative scale for adaptation
% jlev - number of jleves
% Jmx  - maximum allowable level of resolution

% Written: Oleg V. Vasilyev, 11 October, 2018

ende = length(F_in);
D_present   = logical( zeros(jlev_in,1 ));         
D_present(1) = true;                            %Initialize to true because lowest level (C) all are kept.
for j = 1:jlev_in-1
    s     = 2^(jlev_in-j-1);
    D_ind = [1+s:2*s:ende];
    
    %CHECK IF ANY WAVELET IS SIGNIFICANT ON THIS LEVEL
    D_present(j+1) = any(L_in(D_ind));
end

%ADD ADJACENT WAVELETS
jlev_out = max(jlev_in-1,min(max(find(D_present))+1,Jmx)); %ADJUST BY ONE LEVEL AT A TIME

if jlev_out > jlev_in
    s                   = 2^(jlev_out-jlev_in);              %Stride
    nprd                = mod(ende, 2);
    ende                = s*(ende-nprd)+nprd;
    F_out               =          zeros(1, ende)  ;
    L_out               = logical( zeros(1, ende) );
    
    C_ind               = 1:s:ende;                         %The C's of the new finest level, the D's remain zero
    
    %CREATE THE LARGER ARRAYS
    F_out(C_ind)       = F_in;
    L_out(C_ind)       = L_in;
 
elseif jlev_out == jlev_in
    F_out              = F_in;
    L_out              = L_in;
elseif jlev_out < jlev_in
    s                  = 2^(jlev_in-jlev_out);              %Stride
    C_ind              = 1:s:ende;                         %The C's of the new finest level

    %NEW SIZE AND ARRAYS
    nprd                = mod(ende, 2);
    ende               = (ende-nprd)/s+nprd;
    F_out              =          zeros(1, ende)  ;
    L_out              = logical( zeros(1, ende) );
    
    %CREATE THE LARGER ARRAYS
    F_out              = F_in(C_ind);
    L_out              = L_in(C_ind);
end
