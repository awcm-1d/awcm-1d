%THIS SCRIPT TESTS THE WAVELET TRANSFORM WFT ON AN INTERVAL
% Written: Oleg V. Vasilyev, 14 October, 2018

clear ALL;

Type        = 'interval';
%SPECIFY RESOLUTION
cl          = 2^3;                                %RESOLUTION ON COARSEST LEVEL
jlev        = 3;                                  %NUMBER OF LEVELS
Jmx         = 5;                                  %MAXIMUM NUMBER OF LEVELS ALLOWED
jlev_ori    = jlev;
n_ori       = 1+cl*2^(jlev-1);                    %RESOLUTION ON FINEST LEVEL - THIS IS UNIQUE POINTS, THE PERIODIC ONE IS DROPPED
order       = 3; 
acc         = 0.00001;                                %ACCURACY CONTROL OVER WAVELET AMPLITUDES
Flag        = 'forward';
L_ori       = logical(ones(1,n_ori));

%SPECIFY SIGNAL
X_vec_ori   = 0:1/(n_ori-1):1;                    %X-grid on finest level
a      = [2.0,1.0,1.0];
b      = [0.5,0.25,0.75];
m      = [0, 1, 4];
m      = max(0, m);
F_ori  = zeros(size(X_vec_ori));
for k = 1:length(a)
    F_ori    = F_ori + a(k)*(X_vec_ori-b(k)).^m(k);
end
%rand('state',0)                                  %Reset random generator
%F_ori       = 1+detrend(cumsum(rand(n_ori,1)))';  %Generate random, but smooth enough signal

scl = max(abs(F_ori(L_ori)));

%FORWARD WAVELET TRANSFORM ==========================================================================================================
Wtf    = wtf(Type, F_ori, jlev_ori, order, Flag, L_ori);

L_sig = significant(Wtf, L_ori, scl, acc, jlev);

[F_ori, L_ori, jtmp, ende]  = adjust_lev(F_ori, L_sig, jlev, Jmx);
[Wtf,   L_sig, jlev, ende]  = adjust_lev(Wtf,   L_sig, jlev, Jmx);
    
L = reconstruction_check(Type, jlev, order, L_sig);

X_vec                  = 0:1/(ende-1):1;    %X-grid on finest level

%DO TOW DIFFERENT RESTORES - ONE TO ALL POINTS - ONE TO ONLY FLAGGED POINTS
clf;
for i=1:2
    if i==1 
        %DO COMPLETE RESTORE  ===============================================================================================
        Wtf(~L) = 0;
        L_all   = logical(ones(size(L)));
        Wtf_inv = wtf(Type, Wtf, jlev, order, 'Inverse', L_all);
    else
        %DO RESTORE ONLY ON FLAGGED POINTS
        Wtf(~L)  = NaN;
        Wtf_inv  = wtf(Type, Wtf, jlev, order, 'Inverse', L);
    end
    

    
    %VISUALIZE SIGNAL ===================================================================================================================
    figure(111);
    subplot(311);
    hold on
    if i== 1
        plot(X_vec(L_ori), F_ori(L_ori), 'k-');
    else
        plot(X_vec(L), Wtf_inv(L), 'rx',  X_vec(L), Wtf_inv(L), 'r-');
    end
    title(['Acc: ', num2str(acc), ' Error ', num2str(max(abs(Wtf_inv(L_ori)-F_ori(L_ori)))),'  Sav: ', num2str(1-sum(L)/ende)]);
    Achsen  = axis;
    axis([0, 1, Achsen(3:4)]);
    
    %VISUALIZE TREE ===================================================================================================================
    subplot(312)
    hold on
    Level   = jlev*ones(size(Wtf_inv));
    for j = jlev-1:-1:1          
        s                   = 2^(jlev-j-1);
        Level(1:2*s:ende)   = j;
    end    
    
    hold on
    if i== 1
        plot(X_vec,    Level,    'kx');
    else
        plot(X_vec(L), Level(L), 'rx');
    end
    set(gca, 'XTick', [], 'YTick', []);
    axis([0, 1, 0, jlev+1]);
    box on;
    title(['Original jlev: ', num2str(jlev_ori), '  New jlev: ', num2str(jlev)]);
    
    %WAIT FOR PROMPT
    subplot(313);
    hold on
    if i==1
        plot(X_vec(L_ori), abs(F_ori(L_ori)-Wtf_inv(L_ori)),'-k');
        title('Error of the original and reconstructed function');
    else
        plot(X_vec(L), abs(F_ori(L)-Wtf_inv(L)),'xr');
    end        
end



