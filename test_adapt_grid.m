%THIS SCRIPT TESTS THE WAVELET TRANSFORM WFT ON AN INTERVAL
% Written:  V. Vasilyev, 14 October, 2018

clear ALL;

cl          = 2^3;                                %RESOLUTION ON COARSEST LEVEL
jlev        = 3;                                  %NUMBER OF LEVELS
jlev_ori    = jlev;
Jmx         = 7;                                  %MAXIMUM NUMBER OF LEVELS ALLOWED
ende        = 1+cl*2^(jlev-1);                    %RESOLUTION ON FINEST LEVEL - THIS IS UNIQUE POINTS
order       = 3; 
acc         = 0.001;                               %ACCURACY CONTROL OVER WAVELET AMPLITUDES
Type        = 'periodic';
switch lower(Type)
case 'periodic'
    ende = ende - 1;
end
L           = logical(ones(1,ende));

%SPECIFY SIGNAL
a      = [1.0, 1.0];
b      = [0.5, 0.5];

c      = [1.e5,0.02];
X_vec  = 0:1/(ende-1):1;            
F_a    = zeros(size(X_vec)); 
for k = 1:length(a)
    F_a    = F_a + a(k)*exp(-(X_vec-b(k)).^2/c(k)^2);
end

for iter = 1:Jmx-jlev+1
    clf;
    Fn  = F_a;
    Fn(~L) = NaN;
    scl    = max(Fn(L))-min(Fn(L));
    
    Wtf   = wtf(Type, Fn, jlev, order, 'forward', L);
    
    L_sig = significant(Wtf, L, scl, acc, jlev);
    
   [Wtf, L_sig, jlev, ende] = adjust_lev(Wtf, L_sig, jlev, Jmx);
    
    L_sig = add_adjacent(Type, L_sig, jlev);
    
    L = reconstruction_check(Type, jlev, order, L_sig);
    
    [G, j_df] = add_ghost(L, Type, jlev, order);
        
    %DO TOW DIFFERENT RESTORES - ONE TO ALL POINTS - ONE TO ONLY FLAGGED POINTS
    X_vec  = 0:1/(ende-1):1;    %X-grid on finest level
    F_a    = zeros(size(X_vec)); 
    for k = 1:length(a)
        F_a    = F_a + a(k)*exp(-(X_vec-b(k)).^2/c(k)^2);  %Resetting analytical function
    end

    for i=1:2
        if i==1
            %DO COMPLETE RESTORE  ===============================================================================================
            Wtf(~L)            = 0;
            L_all  = logical(ones(size(L)));
            Wtf_inv = wtf(Type, Wtf, jlev, order, 'Inverse', L_all);
        else
            %DO RESTORE ONLY ON FLAGGED POINTS
            Wtf(~L) = NaN;
            Fn      = wtf(Type, Wtf, jlev, order, 'Inverse', L);
        end

        %VISUALIZE SIGNAL ===================================================================================================================
        figure(111);
        subplot(311);
        hold on
        if i== 1
            plot(X_vec, F_a, 'k-');
            title(['Acc: ', num2str(acc), ' Error ', num2str(max(abs(Wtf_inv-F_a))),'  Sav: ', num2str(1-sum(L)/ende)]);
        else
            plot(X_vec(L), Fn(L), 'rx',  X_vec(L), Fn(L), 'r-');
        end
         Achsen  = axis;
        axis([0, 1, Achsen(3:4)]);
        
        %VISUALIZE TREE ===================================================================================================================
        subplot(312)
        hold on
        Level   = jlev*ones(size(X_vec));
        for j = jlev-1:-1:1
            s                   = 2^(jlev-j-1);
            Level(1:2*s:ende)   = j;
        end
        
        hold on
        if i== 1
            plot(X_vec,    Level,    'kx');
        else
            plot(X_vec(L), Level(L), 'rx');
        end
        set(gca, 'XTick', [], 'YTick', []);
        axis([0, 1, 0, jlev+1]);
        box on;
        title(['Original jlev: ', num2str(jlev_ori), '  New jlev: ', num2str(jlev)]);
        
        %WAIT FOR PROMPT
        subplot(313);
        hold on
        if i==1
            plot(X_vec, abs(F_a-Wtf_inv),'-k');
            title('Error of the original and reconstructed function');
        else
            plot(X_vec(L), abs(F_a(L)-Fn(L)),'xr');
        end
    end
    input('Please press enter to continue the mesh adaptation');
end

