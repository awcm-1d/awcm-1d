function df = df_pt(f, h, n_l, n_h)
%DF_PT Calculates first derivative of a funciton at a point X_s for uniformly spaced points X_k= k*Delta  
% (k=(s+n_l)...(s+n_h))
%
% f  - array of function values of the size (N=n_h-n_l+1)
% nl - How many point do I want to use to the left  (could be to the right)
% nh - How many point do I want to use to the right (could be to the right)
%
% EXAMPLE
% diff_pt(f(1:3), 0.1, -1, 1) - Calculates deirvative using using central difference assuming 
% the spacing is 0.1
%
% Written: Oleg V. Vasilyev, 4 October, 2018

df = 0.0;
for j=n_l:n_h
    df=df+f(j-n_l+1)*dwgh(j,n_l,n_h,h);
end
