function [dF, d2F] = calc_derivative(F, L, Type, jlev, order, h, der_order)
%CALC_DERIVATIVE calculate derivatives on optimal grids.
%
% NOTE: 
% The derivatives must be calculated while the inverse transform is performed.
% Otherwise higher wavelets can pollute smoothness through lifting.
%
% F         is the function 
% L         is the mask where I have F given,
% D         is the derivative mask
% Type      is either internal or periodic
% jlev      is the number of jleves
% order     is of the wavelet
% h         is mesh spacing
% der_order is order of derivative (1 or 2 only)
%
% Written: Oleg V. Vasilyev, 4 October, 2018

%SIZE
ende       = length(L);

%Calculates derivative level
j_df = ones(size(L));
j_df(~L)=0;
n_range = [-1:0]; % just near neighbors to calculate derivatieve level
for j=1:jlev-1
    s = 2^(jlev-j-1);

    D_ind = [1+s:2*s:ende];
    D_ind = D_ind(find(L(D_ind)));
    j_df(D_ind)  = j+1;
end

switch lower(Type)
case 'interval'
    
    for j=1:jlev-1
        s = 2^(jlev-j-1);
        
        D_ind = [1+s:2*s:ende];
        C_ind = [1:2*s:ende];
        D_ind = D_ind(find(L(D_ind)));
        C_ind = C_ind(find(L(C_ind)));
        
        for i = D_ind
            n_range_corr=G_range_corr(i,ende,s,n_range);
            for k = 1:length(n_range_corr)
                j_df(i+(2*n_range_corr(k)+1)*s)=j+1;
            end
        end     
    end

case 'periodic'
    
    for j = 1:jlev-1                                                  
        s = 2^(jlev-j-1);
        
        D_ind = [1+s:2*s:ende];
        C_ind = [1:2*s:ende];
        D_ind = D_ind(find(L(D_ind)));
        C_ind = C_ind(find(L(C_ind)));
        
        for k = 1:length(n_range)
            j_df(prd_corr(D_ind+(2*n_range(k)+1)*s,ende))=j+1;
        end
    end
end

%Add ghost points for derivative calculation
G=L;
n_range = [-fix((der_order+1)/2):fix((der_order+1)/2)];
switch lower(Type)
case 'interval'
    
    for j=1:jlev
        s = 2^(jlev-j);
        
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        
        for i = G_ind(find(j == j_df(G_ind)))
            n_range_corr=G_range_corr(i,ende,s,n_range);
            for k = 1:length(n_range_corr)
                G(i+n_range_corr(k)*s)=true;
            end
        end     
    end

case 'periodic'
    
    for j = 1:jlev
        s = 2^(jlev-j);
        
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        G_ind = G_ind(find(j == j_df(G_ind)));
        for k = 1:length(n_range)
            G(prd_corr(G_ind+n_range(k)*s,ende))=true;
        end
    end
end


%MAKE SURE POINTS THAT ARE NOT NEEDED CAN BE ACCESSED (THEY SHOULD BE ZERO FOR ZERO CONTRIBUTION)
F(~L)  = NaN;   %ONLY UN-NANING

%INITALIZE DERIVATIVE
dF  = NaN*ones(size(L));
d2F  = NaN*ones(size(L));

Fn    = wtf(Type, F, jlev, order, 'forward', L);
%MAKE SURE STUFF THAT IT IS NOT FLAGGED DOES NOT CONTRIBUTE (C's WOULD ACCESS D's ON BOTH SIDES, NOT JUST ON THE ONE THAT DOES EXIST)
Fn(~L)   = 0;

%CALCULATION OF n - THE RESOLUTION ON THE FINEST LEVEL
ende    = length(L);
n_range = [-fix((order+1)/2):order-fix((order+1)/2)];
d_range = [-fix((der_order+1)/2):fix((der_order+1)/2)];

switch lower(Type)
case 'interval'   
    
    %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j=1:jlev
        s = 2^(jlev-j);
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        
        for i = G_ind(find(j == j_df(G_ind)))
            n_range_corr=G_range_corr(i,ende,s,d_range);
            dF(i) = df_pt(Fn(i+n_range_corr*s), h*s, n_range_corr(1), n_range_corr(end));
            d2F(i) = d2f_pt(Fn(i+n_range_corr*s), h*s, n_range_corr(1), n_range_corr(end));
        end

        if j < jlev
            %STRIDE
            s = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(G(D_ind)));
            C_ind = C_ind(find(G(C_ind)));
            
            %INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for i = C_ind
                n_range_corr=C_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) - 0.5*Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
            %INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for i = D_ind
                n_range_corr=D_range_corr(i,ende,s,n_range);
                order_corr = length(n_range_corr) - 1;
                for k = 1:order_corr+1
                    Fn(i) = Fn(i) + Fn(i+(2*n_range_corr(k)+1)*s)*wgh(n_range_corr(k), n_range_corr(1), n_range_corr(order_corr+1));
                end
            end
        end
        
    end
    
    %SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
    Fn(~G)   = NaN;
    
case 'periodic'
    %LOOP THROUGH THE LEVELS - ONE LESS THAN jlev BECAUSE WE DON'T TOUCH THE FINEST LEVEL
    for j=1:jlev
        s = 2^(jlev-j);
        G_ind = [1:s:ende];
        G_ind = G_ind(find(L(G_ind)));
        
        for i = G_ind(find(j == j_df(G_ind)))
            dF(i)  = df_pt(Fn(prd_corr(i+d_range*s,ende)),  h*s, d_range(1), d_range(end));
            d2F(i) = d2f_pt(Fn(prd_corr(i+d_range*s,ende)), h*s, d_range(1), d_range(end));
        end
        
        if j < jlev
            %STRIDE
            s = 2^(jlev-j-1);
            
            %SETUP INDICES, NOTE THAT THE C_ind(end) D(1) ARE ONLY ACCESED IN INTERPOLATION
            D_ind = [1+s:2*s:ende];
            C_ind = [1:2*s:ende];
            
            D_ind = D_ind(find(G(D_ind)));
            C_ind = C_ind(find(G(C_ind)));
            
            %INVERSE UPDATE STAGE: REDISTRIBUTE WEIGHTS
            for k = 1:order+1
                Fn(C_ind) = Fn(C_ind) - 0.5*Fn(prd_corr(C_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end
            
            %INVERSE PREDICT SAGE: CALCULATE D, THE DIFFERENCE BETWEEN THE INTERPOLATION AND THE REAL FUNCTION
            for k = 1:order+1
                Fn(D_ind) = Fn(D_ind) + Fn(prd_corr(D_ind+(2*n_range(k)+1)*s,ende))*wgh(n_range(k), n_range(1), n_range(order+1));
            end
            
        end
        
    end
    
    %SET THE POINTS THAT WERE NOT DESIRED TO NaN -> THEY CANNOT BE USED
    Fn(~G)   = NaN;    
end



