function N_range_corr = G_range_corr(Ind, n, s, N_range)
%G_RANGE_CORR the bound to ensure the points do not go outside the interval
% Written: Oleg V. Vasilyev 11 October, 2018

N_range_corr = Ind+s*N_range;
N_range_corr = N_range_corr + max(1,N_range_corr(1)) - N_range_corr(1);
N_range_corr = N_range_corr + min(n,N_range_corr(end))- N_range_corr(end);  
N_range_corr = (N_range_corr(find(N_range_corr > 0)) - Ind)/s;
